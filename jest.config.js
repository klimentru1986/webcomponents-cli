module.exports = {
  testPathIgnorePatterns: ['node_modules', 'tmp'],
  projects: [
    {
      displayName: 'e2e',
      testMatch: ['<rootDir>/e2e/**/*.js']
    }
  ]
};
