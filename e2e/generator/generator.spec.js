const shell = require('shelljs');
const shellUtils = require('../../src/utils/shellUtils');
const fsUtils = require('../../src/utils/fsUtils');

describe('generator e2e', () => {
  afterEach(() => {
    shell.rm('-rf', 'tmp/generator/*');
  });

  it('should return no ars error when run without params', async () => {
    expect.assertions(1);

    try {
      const res = await shellUtils.exec('node index.js');

      expect(res).toMatchSnapshot();
    } catch (e) {}
  });

  it('should return invalid arguments error when args more then 2', async () => {
    expect.assertions(1);

    try {
      const res = await shellUtils.exec('node index.js arg1 arg2 arg3');

      expect(res).toMatchSnapshot();
    } catch (e) {}
  });

  it('should return invalid generator error when first arg unknown', async () => {
    expect.assertions(1);

    try {
      const res = await shellUtils.exec('node index.js arg1 arg2');

      expect(res).toMatchSnapshot();
    } catch (e) {}
  });

  it('should generate component when first arg = component', async () => {
    expect.assertions(2);

    try {
      await shellUtils.exec('node index.js component tmp/generator/test');
      const component = await fsUtils.readFile('./tmp/generator/test/test.js');
      const template = await fsUtils.readFile('./tmp/generator/test/test.html');

      expect(component).toMatchSnapshot();
      expect(template).toMatchSnapshot();
    } catch (e) {}
  });

  it('should generate component when first arg = c', async () => {
    expect.assertions(2);

    try {
      await shellUtils.exec('node index.js c tmp/generator/test');
      const component = await fsUtils.readFile('./tmp/generator/test/test.js');
      const template = await fsUtils.readFile('./tmp/generator/test/test.html');

      expect(component).toMatchSnapshot();
      expect(template).toMatchSnapshot();
    } catch (e) {}
  });
});
