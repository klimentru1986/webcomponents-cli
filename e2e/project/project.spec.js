const shell = require('shelljs');
const fsUtils = require('../../src/utils/fsUtils');
const shellUtils = require('../../src/utils/shellUtils');

describe('project generator e2e', () => {
  afterEach(() => {
    shell.rm('-rf', 'project');
  });

  it('should generate project', async () => {
    expect.assertions(1);

    try {
      await shellUtils.exec(
        'cd tmp && node ../index.js project project --without-install'
      );
      const folderStructure = fsUtils.getFolderStructure('./tmp/project');

      expect(folderStructure).toMatchSnapshot();
    } catch (e) {}
  });

  it('should generate project with "p" alias', async () => {
    expect.assertions(1);

    try {
      await shellUtils.exec(
        'cd tmp && node ../index.js p project --without-install'
      );
      const folderStructure = fsUtils.getFolderStructure('./tmp/project');

      expect(folderStructure).toMatchSnapshot();
    } catch (e) {}
  });

  it('should return error when project name invalid', async () => {
    try {
      const res = await shellUtils.exec(
        'node index.js p project@@## --without-install'
      );

      expect(res).toMatchSnapshot();
    } catch (e) {}
  });
});
