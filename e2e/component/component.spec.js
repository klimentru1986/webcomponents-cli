const shell = require('shelljs');
const shellUtils = require('../../src/utils/shellUtils');
const fsUtils = require('../../src/utils/fsUtils');

describe('generate component e2e', () => {
  afterEach(() => {
    shell.rm('-rf', 'tmp/comp/*');
  });

  it('should generate component', async () => {
    expect.assertions(2);

    try {
      const res = await shellUtils.exec(
        'node index.js tmp/comp/test --without-html'
      );
      const component = await fsUtils.readFile('./tmp/comp/test/test.js');

      expect(res).toMatchSnapshot();
      expect(component).toMatchSnapshot();
    } catch (e) {}
  });

  it('should generate component with template ', async () => {
    expect.assertions(2);

    try {
      const res = await shellUtils.exec('node index.js tmp/comp/test');
      const component = await fsUtils.readFile('./tmp/comp/test/test.js');
      const template = await fsUtils.readFile('./tmp/comp/test/test.html');

      expect(component).toMatchSnapshot();
      expect(template).toMatchSnapshot();
    } catch (e) {}
  });

  it('should generate component without dir', async () => {
    expect.assertions(2);

    try {
      const res = await shellUtils.exec(
        'node index.js tmp/comp/test --without-dir --without-html'
      );
      const component = await fsUtils.readFile('./tmp/comp/test.js');

      expect(res).toMatchSnapshot();
      expect(component).toMatchSnapshot();
    } catch (e) {}
  });

  it('should return invalid name error', async () => {
    expect.assertions(1);

    try {
      const res = await shellUtils.exec('node index.js tmp/comp/test@#%');
      expect(res).toMatchSnapshot();
    } catch (e) {}
  });
});
