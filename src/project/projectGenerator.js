const path = require('path');
const format = require('string-template');

const { logSuccess } = require('../utils/logUtils');
const nameUtils = require('../utils/nameUtils');
const shellUtils = require('../utils/shellUtils');
const fsUtils = require('../utils/fsUtils');
const { validateProjectName } = require('../utils/nameUtils');
const { generateComponent } = require('../component/componentGenerator');

async function generateProject(projetName, params) {
  validateProjectName(projetName);

  logSuccess('\r\nproject init ... \r\n'.toUpperCase());
  fsUtils.mkDir(projetName);
  const sourceDir = path.join(__dirname, '/templates/template');
  const destDir = path.join(process.cwd(), projetName);
  await fsUtils.copyRecursive(sourceDir, destDir);
  const templateConfig = nameUtils.mapToConfig(projetName);
  await updateTemplates(destDir, templateConfig);

  logSuccess('\r\ninit default component ... \r\n'.toUpperCase());
  await generateComponent(`${projetName}/src/components/${projetName}`, {});

  if (!params.withoutInstall) {
    logSuccess('\r\ninstall packages ... \r\n'.toUpperCase());
    shellUtils.cd(projetName);
    await shellUtils.exec('npm install', { silent: true });
  }
}

async function updateTemplates(projectPath, config) {
  const dir = fsUtils.readDir(projectPath);

  return Promise.all(
    dir.map(async i => {
      const newPath = path.join(projectPath, i);
      if (fsUtils.isDirectory(newPath)) {
        return updateTemplates(newPath, config);
      }

      if (nameUtils.isAsset(i)) {
        return;
      }

      const template = await fsUtils.readFile(newPath);
      const formatedTemplate = format(template, config);
      await fsUtils.writeFile(newPath, formatedTemplate);
      logSuccess(`${newPath} ... SUCCESS`);
    })
  );
}

module.exports = {
  generateProject
};
