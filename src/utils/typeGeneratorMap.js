const { generateComponent } = require('../component/componentGenerator');
const { generateProject } = require('../project/projectGenerator');

const typeGeneratorMap = new Map([
  ['component', generateComponent],
  ['c', generateComponent],
  ['project', generateProject],
  ['p', generateProject]
]);

module.exports = typeGeneratorMap;
