function parseArgs(argv) {
  const { _, $0, ...params } = argv;
  const args = _;

  const result = {};
  result.params = params;

  if (!args.length) {
    throw new Error('Name required!');
  }

  if (args.length > 2) {
    throw new Error(`Invalid arguments length: "${args.join('", "')}"`);
  }

  if (args.length === 1) {
    result.name = args[0];
    return result;
  }

  if (args.length === 2) {
    result.generatorType = args[0];
    result.name = args[1];
    return result;
  }
}

module.exports = parseArgs;
