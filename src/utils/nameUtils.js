const validComponentNameRegexp = /^([A-Za-z\-\_\.\d])+$/;
const validProjectName = /^([A-Za-z\-\_\.\d])+$/;
const assetsRegexp = /\.(png|svg|jpg|gif|woff|woff2|eot|ttf|otf)$/;

function validate(name) {
  if (!name || validComponentNameRegexp.test(name)) {
    return true;
  }

  throw new Error(
    'Component name may only include letters, numbers, underscores, hashes and dots.'
  );
}

function validateProjectName(name) {
  if (!name || validProjectName.test(name)) {
    return true;
  }

  throw new Error('Project name may only include letters, numbers, underscores, hashes and dots.');
}

function isAsset(name) {
  return assetsRegexp.test(name);
}

function mapToConfig(name) {
  const selector = getSelector(name);

  return {
    name,
    selector,
    className: getClassNameFromSelector(selector)
  };
}

function getSelector(name) {
  const res = name
    .replace(/[\_\.]/g, x => '-')
    .replace(/[a-z][A-Z]/g, x => `${x[0]}-${x[1]}`)
    .toLowerCase();

  return /\-/.test(res) ? res : `wcomp-${res}`;
}

function getClassNameFromSelector(selector) {
  return selector
    .replace(/\-[a-z]/g, x => x[1].toUpperCase())
    .replace(/^[a-z]/g, x => x[0].toUpperCase());
}

module.exports = {
  validate,
  validateProjectName,
  mapToConfig,
  isAsset
};
