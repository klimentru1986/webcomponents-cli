const shell = require('shelljs');
const util = require('util');

const exec = util.promisify(shell.exec);
const cd = shell.cd;

module.exports = { exec, cd };
