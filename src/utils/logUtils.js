const chalk = require('chalk');

function logSuccess(text) {
  console.log(chalk.green(text));
}

function logError(text) {
  console.log(chalk.red(text));
}

module.exports = {
  logSuccess,
  logError
};
