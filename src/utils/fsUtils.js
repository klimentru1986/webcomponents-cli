const fs = require('fs');
const p = require('path');
const util = require('util');
const shell = require('shelljs');
const fsExtra = require('fs-extra');

async function readFile(path) {
  const readFile = util.promisify(fs.readFile);

  return await readFile(path, {
    encoding: 'utf8'
  });
}

async function writeFile(path, data) {
  const write = util.promisify(fs.writeFile);

  return await write(path, data, {
    encoding: 'utf8'
  });
}

function mkDir(dir) {
  shell.mkdir('-p', dir);
}

function readDir(path) {
  return fs.readdirSync(path);
}

function isDirectory(path) {
  return fs.statSync(path).isDirectory();
}

function getFolderStructure(projectPath, startString = '') {
  const dir = readDir(projectPath);

  return dir.reduce((acc, i) => {
    const newPath = p.join(projectPath, i);
    if (isDirectory(newPath)) {
      return getFolderStructure(newPath, acc);
    }

    return `${acc}\r\n${newPath}`;
  }, startString);
}

async function copyRecursive(src, dest) {
  const copy = util.promisify(fsExtra.copy);
  return await copy(src, dest);
}

module.exports = {
  readFile,
  writeFile,
  copyRecursive,
  mkDir,
  readDir,
  isDirectory,
  getFolderStructure
};
