const yargs = require('yargs');
const { logSuccess, logError } = require('./utils/logUtils');

const typeGeneratorMap = require('./utils/typeGeneratorMap');
const argsParser = require('./utils/argsParser');

generate().then(
  () => logSuccess('\r\nCompleted!!!\r\n'.toUpperCase()),
  err => logError(err)
);

async function generate() {
  const args = argsParser(yargs.argv);

  if (!args.generatorType) {
    const generator = typeGeneratorMap.get('c');
    return await generator(args.name, args.params);
  }

  const generator = typeGeneratorMap.get(args.generatorType);

  if (!generator) {
    throw new Error(`Invalid argument "${args.generatorType}"`);
  }

  return await generator(args.name, args.params);
}
