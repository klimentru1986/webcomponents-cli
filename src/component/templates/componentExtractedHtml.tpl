import tmpl from './{name}.html';

const template = document.createElement('template');
template.innerHTML = tmpl;

export class {className} extends HTMLElement {

  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define('{selector}', {className});
