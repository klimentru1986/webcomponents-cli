const path = require('path');
const format = require('string-template');
const { logSuccess } = require('../utils/logUtils');

const fsUtils = require('../utils/fsUtils');
const nameUtils = require('../utils/nameUtils');
const { templateNames, templateExtMap } = require('./templateNamesConst');

async function generateComponent(componentName, params) {
  if (params.withoutHtml) {
    return await generateFromTemplate({
      componentName,
      templateName: templateNames.component,
      params
    });
  } else {
    const templates = [
      templateNames.componentExtractedHtml,
      templateNames.componentHtml
    ];
    return Promise.all(
      templates.map(
        async templateName =>
          await generateFromTemplate({
            componentName,
            templateName,
            params
          })
      )
    );
  }
}

async function generateFromTemplate({ componentName, templateName, params }) {
  const compPath = componentName.split('/');
  componentName = compPath.slice(-1)[0];

  const ext = templateExtMap.get(templateName);

  nameUtils.validate(componentName);

  const template = await fsUtils.readFile(
    path.join(__dirname, `/templates/${templateName}.tpl`)
  );

  const formatedTemplate = format(
    template,
    nameUtils.mapToConfig(componentName)
  );

  if (params.withoutDir) {
    compPath.splice(-1, 1);
  }

  const compPathString = compPath.join('/');
  fsUtils.mkDir(compPathString);
  const res = await fsUtils.writeFile(
    path.join(process.cwd(), `${compPathString}/${componentName}.${ext}`),
    formatedTemplate
  );

  logSuccess(`${compPathString}/${componentName}.${ext} ... SUCCESS`);

  return res;
}

module.exports = {
  generateComponent
};
