const templateNames = {
  component: 'component',
  componentHtml: 'componentHtml',
  componentExtractedHtml: 'componentExtractedHtml'
};

const templateExtMap = new Map([
  [templateNames.component, 'js'],
  [templateNames.componentExtractedHtml, 'js'],
  [templateNames.componentHtml, 'html']
]);

module.exports = {
  templateNames,
  templateExtMap
};
